<?php

/*
 * appSync: code to sync objects between a server and mobile devices (clients)
 * e.g. in order to implement mobile apps with offline usage, or to integrate in an mBaas platform
 * version: 0.1
 * date: 18/07/2014
 *
*/

define("OK", 1);
define ("NOK", 0);

define("TIMESTAMPPRIORITY", 1); //Conflict handling: version with most recent timestamp_lastupdate is used
define("SERVERPRIORITY", 2); //Conflict handling: version from server is used
define("CLIENTPRIORITY", 3); //Conflict handling: version from client is used

//Note on conflict handling with client priority: this should NOT be used in case the same PK (primary key) can be created on multiple clients,
//because this will result in a PK conflict that will never be resolved, both clients will continue "fighting" for their version of the object in consecutive syncs.
//This can be resolved by adding a property object.source which keeps track of the source (e.g. "client1") where this object was created.
//Client priority should only be invoked when the object is created on the client itself, otherwise the server version must be used.

class Server
{
    public $name = "Server";
    private $counter = 0;
    public $objects = array();

    public function __constructor($name)
    {
        $this->name = $name;
    }

    /* called by client->syncFromServer(), in reality using REST API call */
    public function syncToClient($ServerCounterSyncStart, &$objectsToSync)
    {
        //echo "Server received request to sync to client starting at $ServerCounterSyncStart";
        $objectsToSync = array();
        foreach ($this->objects as $object)
        {
            if ($object->counter_lastupdate > $ServerCounterSyncStart)
            {
                $objectsToSync[] = $object;
            }
        }
        $result["statuscode"] = OK;
        $result["servercounter"] = $this->counter;
        return $result;
    }

    /* called by client->syncToServer, in reality using REST API call */
    public function syncFromClient($objectsToSync)
    {
        foreach ($objectsToSync as $objectToSync)
        {
            //update local object based on guid
            $objectExists = false;
            $pkConflict = false;
            foreach ($this->objects as $object)
            {
                if ($object->guid == $objectToSync->guid)
                {
                    $objectExists = true;
                    $object->value = $objectToSync->value;
                    $object->isdeleted = $objectToSync->isdeleted;
                    $this->counter++; //we must increase the counter, because other clients must also receive these updates
                    $object->counter_lastupdate = $this->counter;
                }
                elseif ($object->pk == $objectToSync->pk) //this means object with other guid but with same PK (primary key)
                {
                    //PK conflict: do nothing, because this should not occur on server
                    //client will always sync from server to client first and handle any PK conflicts, before syncing from client to server
                    $objectExists = true;
                    $pkConflict = true;
                    $result["statuscode"] = NOK;
                }
            }
            if (!$objectExists)
            {
                $newObject = new Object($objectToSync->pk, $objectToSync->name, $objectToSync->value, $objectToSync->guid);
                $newObject->isdeleted = $objectToSync->isdeleted;
                $this->counter++; //we must increase the counter, because other clients must also receive these updates
                $newObject->counter_lastupdate = $this->counter;
                $this->objects[] = $newObject;
            }
        }
        if (!isset($result["statuscode"]))
            $result["statuscode"] = OK;
        $result["servercounter"] = $this->counter;
        return $result;
    }

    /* Create object on server (do not use this function to add object from a client sync) */
    public function addObject($pk, $name, $value) //try to move all this into Object class (and access parent to get counter)
    {
        $newObject = new Object($pk, $name, $value);
        $this->counter++;
        $newObject->counter_lastupdate = $this->counter;
        $this->objects[] = $newObject;
    }

    /* Update object on server (do not use this function to update object from a client sync) */
    public function updateObject($pk, $newValue)
    {
        foreach ($this->objects as $object)
        {
            if ($object->pk == $pk)
            {
                $object->update($newValue);
                $this->counter++;
                $object->counter_lastupdate = $this->counter;
            }
        }
    }

    /* Delete object on server (do not use this function to delete object from a client sync) */
    public function deleteObject($pk)
    {
        foreach ($this->objects as $object)
        {
            if ($object->pk == $pk)
            {
                $object->delete();
                $this->counter++;
                $object->counter_lastupdate = $this->counter;
            }
        }
    }

    function debugOutput($text, $objects)
    {
        echo $text . "\n";
        foreach ($this->objects as $object)
        {
            $object->display();
        }
        echo " -----------------------------------\n\n";
    }

    function display()
    {
        echo " State of server: " . $this->name . " - ";
        echo "Counter: " . $this->counter . "\n";
        $this->debugOutput(" Objects on server:", $this->objects);
    }
}

class Client
{
    public $name;
    public $counter = 0;
    public $counter_lastsync = 0; //value of $this->counter when last sync to server was done ($this->syncToServer)
    public $servercounter_lastsync = 0; //value of server counter when last sync from server was done ($this->syncFromServer)
    public $objects = array();

    public function __construct($name)
    {
        $this->name = $name;
    }

    private function syncFromServer()
    {
        global $conflictHandling, $server;

        $objectsToSync = array();
        $result = $server->syncToClient($this->servercounter_lastsync, $objectsToSync); //in reality server will be called using an HTTPS call to a REST API

        $this->debugOutput(" Sync from server to client " . $this->name . " - received objects:", $objectsToSync);

        foreach ($objectsToSync as $objectToSync)
        {
            //update local object based on guid, handle conflict
            $objectExists = false;
            foreach ($this->objects as $object)
            {
                if ($object->guid == $objectToSync->guid || $object->pk == $objectToSync->pk)
                {
                    $objectExists = true;

                    //handle PK conflict
                    if ($object->pk == $objectToSync->pk)
                    {
                        $object->guid = $objectToSync->guid; //merge objects
                    }

                    //check for conflict (object updated locally since last sync to server)
                    if ($object->counter_lastupdate > $this->counter_lastsync)
                    {
                        //decide how to handle conflict
                        if ($conflictHandling == SERVERPRIORITY)
                        {
                            $object->value = $objectToSync->value;
                            $object->isdeleted = $objectToSync->isdeleted;
                        }
                        elseif ($conflictHandling == CLIENTPRIORITY)
                        {
                            //no change to local object
                        }
                        elseif ($conflictHandling == TIMESTAMPPRIORITY)
                        {
                            if ($objectToSync->timestamplastupdate > $object->timestamplastupdate)
                            {
                                $object->value = $objectToSync->value;
                                $object->isdeleted = $objectsToSync->isdeleted;
                                $object->timestamplastupdate = time();
                            }
                        }
                    }
                    else //no conflict: update object locally
                    {
                        $object->value = $objectToSync->value;
                        $object->isdeleted = $objectToSync->isdeleted;
                    }
                }
            }
            if (!$objectExists)
            {
                $newObject = new Object($objectToSync->pk, $objectToSync->name, $objectToSync->value, $objectToSync->guid);
                $newObject->isdeleted = $objectToSync->isdeleted;
                $newObject->counter_lastupdate = $this->counter; //do not increase $this->counter because no change that must be synced back to server
                $this->objects[] = $newObject;
            }
        }
        if ($result["statuscode"] == OK)
        {
            $this->servercounter_lastsync = $result["servercounter"];
        }
    }

    private function syncToServer()
    {
        global $server;

        $objectsToSync = array();
        foreach ($this->objects as $object)
        {
            //object changed on client since last sync to server ?
            if ($object->counter_lastupdate > $this->counter_lastsync)
            {
                $objectsToSync[] = $object;
            }
        }

        $this->debugOutput(" Sync to server - objects to send to server:", $objectsToSync);

        $result = $server->syncFromClient($objectsToSync); //in reality server will be called using an HTTPS call to a REST API
        if ($result["statuscode"] == OK)
        {
            $this->counter_lastsync = $this->counter; //better to store this->counter in $currentCounter at beginning of sync
            $this->servercounter_lastsync = $result["servercounter"]; //because this sync will have increased the server counter and otherwise client will receive its own update on next sync

            //optionally you can add a check here: client can only sync to server when client has done sync from server first:
            //this is to avoid that client would miss out on server updates because of the above line
        }
    }

    public function doSync()
    {
        //first sync from server to client, then from client to server,
        //because only client can handle conflicts (server cannot handle conflicts because server does not know state of client)
        $this->syncFromServer();
        $this->syncToServer();
    }

    public function doFullSync()
    {
        $this->counter_lastsync = 0; //force full sync to server
        $this->servercounter_lastsync = 0; //force full sync from server
        $this->doSync();
    }

    /* Create object on client (do not use this function to add object from a server sync) */
    public function addObject($pk, $name, $value)
    {
        //check if PK (primary key) not in use yet
        foreach ($this->objects as $object)
        {
            if ($object->pk == $pk)
            {
                echo "Error creating new object on client " . $this->name . ": primary key " . $pk . " already in use!";
                exit();
            }
        }

        $newObject = new Object($pk, $name, $value);
        $this->counter++;
        $newObject->counter_lastupdate = $this->counter;
        $this->objects[] = $newObject;
    }

    /* Update object on client (do not use this function to add object from a server sync) */
    public function updateObject($pk, $newValue)
    {
        foreach ($this->objects as $object)
        {
            if ($object->pk == $pk)
            {
                $object->update($newValue);
                $this->counter++;
                $object->counter_lastupdate = $this->counter;
            }
        }
    }

    /* Delete object on client (do not use this function to delete object from a server sync) */
    public function deleteObject($pk)
    {
        foreach ($this->objects as $object)
        {
            if ($object->pk == $pk)
            {
                $object->delete();
                $this->counter++;
                $object->counter_lastupdate = $this->counter;
            }
        }
    }

    function debugOutput($text, $objects)
    {
        echo $text . "\n";
        if (!isset($objects) || count($objects) == 0)
            echo " None\n";
        else
            foreach ($objects as $object)
            {
                $object->display();
            }
        echo " -----------------------------------\n\n";
    }

    function display()
    {
        echo " State of client: " . $this->name . " - ";
        echo "Counter: " . $this->counter . " - ";
        echo "Counter last sync: " . $this->counter_lastsync . " - ";
        echo "Server counter last sync: " . $this->servercounter_lastsync . "\n";
        $this->debugOutput(" Objects on client:", $this->objects);
    }

}

class Object
{
    public $guid;
    public $pk; //primary key
    public $name;
    public $value;
    public $timestampcreated;
    public $timestamlastupdate;
    public $isdeleted = false;
    public $counter_lastupdate = 0;

    function __construct($pk, $name, $value, $guid = null)
    {
        if ($guid == null)
        {
            $this->guid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        }
        else
        {
            $this->guid = $guid;
        }
        $this->pk = $pk;
        $this->name = $name;
        $this->value = $value;
        $this->timestampcreated = time();
        $this->timestamplastupdate = time(); //is not updated during sync, only when object is updated directly on client or server
    }

    function update($newvalue)
    {
        $this->value = $newvalue;
        $this->timestamplastupdate = time(); //is not updated during sync, only when object is updated directly on client or server
    }

    function delete()
    {
        $this->isdeleted = 1;
        $this->timestamplastupdate = time(); //is not updated during sync, only when object is updated directly on client or server
    }

    function display()
    {
        echo " guid: " . $this->guid . " - ";
        echo "pk: " . $this->pk . " - ";
        echo "name: " . $this->name . " - ";
        echo "value: " . $this->value . " - ";
        echo "timestamp last update: " . $this->timestamplastupdate . " - ";
        echo "counter last update: " . $this->counter_lastupdate;
        echo "\n";
    }
}

?>


