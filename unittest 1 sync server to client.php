<?php
/**
 * appSync unit tests for sync between clients and server
 * version: 0.1
 * date: 18/07/2014
 *
 */

include("appsync.php");
$conflictHandling = TIMESTAMPPRIORITY;

$server = new Server("server");
$client1 = new Client("client1", $server);
$client2 = new Client("client2", $server);

echo "*** START Creating apples on server\n";
$server->addObject("2014-05-10", "apples", "3");
echo "*** END   Creating apples on server\n";
$server->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Sync again on client 1 (no data should be synced) \n";
$client1->doSync();
echo "*** END Sync again on client 1 (no data should be synced) \n";
$client1->display();
$server->display();

echo "*** START Updating apples on server\n";
$server->updateObject("2014-05-10", "5");
echo "*** END   Updating apples on server\n";
$server->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();