<?php
/**
 * appSync unit tests for sync between clients and server
 * version: 0.1
 * date: 18/07/2014
 *
 */

include("appsync.php");
$conflictHandling = TIMESTAMPPRIORITY;

$server = new Server("server");
$client1 = new Client("client1", $server);

echo "*** START Creating apples on client 1\n";
$client1->addObject("2014-05-10", "apples", "3");
echo "*** END   Creating apples on client 1\n";
$client1->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "*** START Deleting apples on client 1\n";
$client1->deleteObject("2014-05-10");
echo "*** END   Deleting apples on client 1\n";
$client1->display();

echo "*** START Do sync on client 1\n";
$client1->doSync();
echo "*** END   Do sync on client 1\n";
$client1->display();
$server->display();

echo "Object on server: isdeleted=" . $server->objects[0]->isdeleted;